#!/usr/bin/env bash

# Image Caption Extractor - Recursive
# Created by Brendan Kristiansen on 4 September 2018
# Updated on 11 September 2019

for obj in ./*;
do
	if [ ${obj: -4} == ".jpg" ]
	then
		echo "Extracting caption from $obj"
		ice-single $obj
	elif [ ${obj: -4} == ".JPG" ]
	then
		echo "Extracting caption from $obj"
		ice-single $obj
	elif [ -d $obj ]
	then
		cd $obj
		echo "Moved to $(pwd)"

		if [ -e captions.txt ]
		then
			rm -v captions.txt
		fi

		ice-recursive
		cd ..
	fi
done
