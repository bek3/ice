#!/usr/bin/env bash

# Image Caption Extractor - Recursive
# Created by Brendan Kristiansen on 4 August 2018
# Updated on 11 September 2019

if [ -e captions.txt ]
then
	rm -v captions.txt
fi

for file in ./*;
do
	if [ ${file: -4} == ".jpg" ]
	then
		echo "Extracting caption from $file"
		ice-single $file
	elif [ ${file: -4} == ".JPG" ]
	then
		echo "Extracting caption from $file"
		ice-single $file

    fi
done
