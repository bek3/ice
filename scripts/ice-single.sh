#!/usr/bin/env bash

# Image Caption Extractor
# Created by Brendan Kristiansen on 4 August 2018
# Updated on 4 August 2018

if command -v identify > /dev/null;
then
	filename=$1

	echo "$filename:" >> captions.txt

	identify -verbose $filename | grep "exif:ImageDescription" >> captions.txt
else
	echo "Requires ImageMagick package installed"
fi
