# ice

ICE (Image Caption Extractor) is a tool used to extract the EXIF captions stored in digital images.

## Dependencies

* ImageMagick

## Compatibility

ICE currently supports the following image formats:

* Jpeg (.jpg)

## Usage

* `ice-single $filename` - Run ICE on a single file
* `ice-all` - Run ICE on every file in the current working directory
* `ice-recursive` - Run ICE recursively on every file in the current working directory and every directory below
