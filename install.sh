#!/usr/bin/env bash

# Image Caption Extractor Installer
# Created by Brendan Kristiansen on 4 August 2018
# Updated on 4 September 2018

if command -v identify > /dev/null;
then
	
	echo "Image Capture Extractor"
	if [ ! -d $HOME/bin ]
	then
		mkdir -v $HOME/bin
	fi

	sudo cp -v scripts/ice-all.sh ~/bin/ice-all
	sudo cp -v scripts/ice-single.sh ~/bin/ice-single
	sudo cp -v scripts/ice-recursive.sh ~/bin/ice-recursive

	sudo chmod 755 ~/bin/ice-all
	sudo chmod 755 ~/bin/ice-single
	sudo chmod 755 ~/bin/ice-recursive

	echo "ICE successfully installed"
else
	echo "ImageMagick required for ICE"
fi
